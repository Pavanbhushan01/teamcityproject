﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TeamCityProject.Classes
{
    public class MyDetails
    {

        public MyDetails(int id, string name, string   city)
        {
            this.Id = id;
            this.Name = name;
            this.City = city;
        }
        public int Id { get; set; }
        public string Name { get; set; }

        public string City { get; set; }


    }
}
